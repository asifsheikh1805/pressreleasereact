import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import ReactQuill from 'react-quill';
import PressForm from './PressForm';
import Preview from './Preview';
import { alertActions } from '../_actions';
import { landingPageServices } from '../_services';

const mapStateToProps = state => {
  return { showPreview: state.showPreview };
};

const Font = ReactQuill.Quill.import('formats/font');
Font.whitelist = ['balkeno','opensans-regular', 'roboto', 'sapient-sans', false] ; // allow ONLY these fonts and the default
ReactQuill.Quill.register(Font, true);

const Image = ReactQuill.Quill.import('formats/image');
Image.sanitize = function(url) {
  	return url; // You can modify the URL here
};

class Editor extends React.Component {
  constructor (props) {
    super(props);

    this.state = { editorHtml: '', 
      theme: 'snow', 
      file: null,
    }
    /*Local Copy of Data for removing overriding issues*/
    this.temp = {
        'press-title': '',
        'press-url': '',
        'meta-title': '',
        'meta-desc': '',
        'meta-keywords': '',
        'editorHtml': ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
  }
  
  handleChange (data) {
    const { pressData } = this.state;

    if(typeof data === 'object'){
      this.setState({ pressData: {...pressData, ...data}
      });
      this.temp = {...this.temp, ...data};
    } else {
      let regex = /(<([^>]+)>)/ig;
      let content = data.replace(regex, "");
      if(content){
          this.setState({ editorHtml : data
          });
          this.temp['editorHtml'] = data;
      } else {
          this.setState({ editorHtml : content
          });
          this.temp['editorHtml'] = content;
      }
    }

    this.props.editorHtmlTemplate(this.temp);
  }
  
  handleFileChange(event) {
    /*this.setState({
      file: URL.createObjectURL(event.target.files[0])
    });*/

    const { dispatch } = this.props;
    var range = this.quillRef.getEditor().getSelection();
    var reader = new FileReader();
    var params = {};
    var imageFiles = event.target.files[0];

    reader.readAsDataURL(imageFiles);
    reader.onload = () => {
      params['temp-path'] = reader.result;
      params['name'] = imageFiles.name;
      params['size'] = imageFiles.size;
      params['type'] = imageFiles.type;

      landingPageServices.imageUpload(params)
      .then(
        data => {
          if(data.isSuccess){
            /*var value = URL.createObjectURL(imageFiles);*/
            var value = window.ctx + data.attachmentUrl;
            if(value) {
                this.quillRef.getEditor().insertEmbed(range.index, 'image', value, "user");
            }
          } else {
            dispatch(alertActions.error(data.errMsg));
          }
        },
        error => {
          dispatch(alertActions.error(error.message));
        }
      );
    };
  }

  componentWillMount() {
   	this.setState({ editorHtml : this.props.placeholder });
    this.temp = {...this.temp, ...this.props.pressData};
  }

  componentDidMount() {
    const toolbar = this.quillRef.getEditor().getModule('toolbar');
    toolbar.addHandler('image', ()=>{this.fileUpload.click()});
  }

  render () {
    const { showPreview } = this.props;

    return (
      <div>
      	<PressForm pressInfoData={this.handleChange} />
        { showPreview.toggleViewStatus ? <div>
            <ReactQuill 
              theme={this.state.theme}
              onChange={this.handleChange}
              defaultValue={this.state.editorHtml}
              modules={Editor.modules}
              formats={Editor.formats}
              bounds={'.app'}
              placeholder={this.state.editorHtml} 
              ref={(el) => this.quillRef = el} />
            <input type="file" style={{ display: "none" }} onChange={this.handleFileChange} ref={(el) => this.fileUpload = el} />
        </div> : <Preview/>}
       </div>
     )
  }
}

/* 
 * Quill modules to attach to editor
 * See https://quilljs.com/docs/modules/ for complete options
 */
Editor.modules = {
  toolbar: {
     container:  [
		[{ 'header': [1, 2, 3]}, { 'font': Font.whitelist }],
		[{size: []}],
		['bold', 'italic', 'underline', 'strike', 'blockquote'],
		[{'list': 'ordered'}, {'list': 'bullet'}, 
		{'indent': '-1'}, {'indent': '+1'}],
		['link', 'video'],['image'],
		['clean']
	]
  },
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  }
}

/* 
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
Editor.formats = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image', 'video'
]

/* 
 * PropType validation
 */
Editor.propTypes = {
  placeholder: PropTypes.string,
}

export default connect(mapStateToProps,null)(Editor);