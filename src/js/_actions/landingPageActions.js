import { alertActions } from './';
import { landingPageConstants } from '../_constants/action-types';
import { landingPageServices } from '../_services';

export const showPressPreview = showPreviewHtml => ({ 
	type: landingPageConstants.SHOW_PRESS_PREVIEW, 
	showPreviewHtml: showPreviewHtml
});

export const toggleView = toggleViewStatus => ({ 
	type: landingPageConstants.TOGGLE_VIEW, 
	toggleViewStatus: toggleViewStatus
});

export function registerPress(pressData) {
	const success = (pressData) => { return {type: landingPageConstants.PRESS_REGISTER_SUCCESS, pressData}};
	const register = (pressData) => { return {type: landingPageConstants.PRESS_REGISTER_REQUEST, pressData}};
	const failure = (error) => { return {type: landingPageConstants.PRESS_REGISTER_FAILURE, error}};

	return dispatch => {
		dispatch(register(pressData));

		landingPageServices.register(pressData)
		.then(
			data => {
				if(data.isSuccess){
					dispatch(success(data));
					dispatch(alertActions.success("Posted Successfully"));
					window.location.href = window.ctx + '/index/core?press';
				} else {
					dispatch(failure(data));
					dispatch(alertActions.error(data.errMsg));
				}
			},
			error => {
				dispatch(failure(error));
				dispatch(alertActions.error(error.message));
			}
		);
	};

}