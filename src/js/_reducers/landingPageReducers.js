import { landingPageConstants } from "../_constants/action-types";
const initialState = {
  showPreviewHtml: '',
  toggleViewStatus: true
};

export function showPreview(state = initialState, action){
  switch (action.type) {
    case landingPageConstants.SHOW_PRESS_PREVIEW:
      return { ...state, showPreviewHtml: action.showPreviewHtml};
    case landingPageConstants.TOGGLE_VIEW:
      return { ...state, toggleViewStatus: action.toggleViewStatus};
    default:
      return state;
  }
}

export function registration(state = {}, action){
  switch (action.type) {
    case landingPageConstants.PRESS_REGISTER_REQUEST:
      return { registering: true };
    case landingPageConstants.PRESS_REGISTER_SUCCESS:
      return {};
    case landingPageConstants.PRESS_REGISTER_FAILURE:
      return { /*registering: false */};
    default:
      return state;
  }
}