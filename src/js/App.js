import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from '../logo.png';
import { alertActions } from './_actions';
import '../css/App.css';
import '../css/common.css';
import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.bubble.css';
import Editor from './components/TextEditor';
import { showPressPreview, toggleView, registerPress } from './_actions';

const mapStateToProps = (state) => ({
  alert: state.alert,
  showPreview: state.showPreview
});

class App extends Component {
  constructor (props) {
    super(props);

    this.state = { 
      editorHtml: '',
      disablePreviewButton: true,
      disableSaveButton: true,
      pressData: {
          'press-title': '',
          'press-url': '',
          'meta-title': '',
          'meta-desc': '',
          'meta-keywords': '',
          'editorHtml': ''
      },
      submitted: false,
    }
    this.editorHtmlTemplate = this.editorHtmlTemplate.bind(this);
    this.showPreviewPage = this.showPreviewPage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissAlert = this.dismissAlert.bind(this);
  }
  
  editorHtmlTemplate (data) {
    const { pressData } = this.state;

    this.setState({ pressData: {...pressData, ...data} 
    });

    if(data['editorHtml']){
      this.setState({ disablePreviewButton: false });
    } else {
      this.setState({ disablePreviewButton: true });
    }

    if(data['press-title'] && data['meta-title'] && data['meta-desc']
      && data['meta-keywords'] && data['editorHtml']){
      this.setState({ disableSaveButton: false });
    } else {
      this.setState({ disableSaveButton: true });
    }
  }

  showPreviewPage(){
    const { dispatch, showPreview } = this.props;
    if(showPreview.toggleViewStatus){
      dispatch(showPressPreview(this.state.pressData.editorHtml));
      dispatch(toggleView(!showPreview.toggleViewStatus));
    } else {
      dispatch(toggleView(!showPreview.toggleViewStatus));
    }
  }

  handleSubmit(event) {
      event.preventDefault();
      this.setState({ submitted: true });
      const { pressData } = this.state;
      const { dispatch } = this.props;

      dispatch(registerPress(pressData));
  }

  dismissAlert() {
    const { dispatch } = this.props;
    dispatch(alertActions.clear())
  }

  render() {
    const { alert, showPreview } = this.props;
    let previewbtnopts = {};
    let savebtnopts = {};
    if (this.state.disablePreviewButton) {
        previewbtnopts['disabled'] = 'disabled';
    }

    if (this.state.disableSaveButton) {
        savebtnopts['disabled'] = 'disabled';
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        {alert.message &&
          <div className="alert-container">
            <div className={`alert ${alert.type}`}>
                  {alert.message}
              <button type="button" className="close" onClick={this.dismissAlert}>
                <span>×</span>
              </button>
            </div>
          </div>
        }
        <Editor placeholder={showPreview.showPreviewHtml} editorHtmlTemplate={this.editorHtmlTemplate}/>
        <div className="buttonContainer">
          <button className="previewPage success" {...previewbtnopts} onClick={this.showPreviewPage}> 
            { showPreview.toggleViewStatus ? 'Preview' : 'Back' } 
          </button>
          <button className="save primary" {...savebtnopts} onClick={this.handleSubmit}> Save </button>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(App);
